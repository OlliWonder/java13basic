package week10;

import java.util.Arrays;

public class CompareSort {
    
    //быстрая сортировка
    public static void javaSort(int[] array) {
        long startTime = System.nanoTime();
        Arrays.sort(array);
        long stopTime = System.nanoTime();
        System.out.println((double)(stopTime - startTime) / 1000000000);
    }
    
    public static void bubbleSort(int[] array) { //первая реализация
        long startTime = System.nanoTime();
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length - i; j++) {
                if (array[j - 1] > array[j]) {
                    int temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        long stopTime = System.nanoTime();
        System.out.println((double)(stopTime - startTime) / 1000000000);
    }
    
    //вторая реализация
    public static void bubbleSort2(int[] array) {
        long startTime = System.nanoTime();
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 1; i < array.length; i++) {
                if (array[i - 1] > array[i]) {
                    int temp = array[i - 1];
                    array[i - 1] = array[i];
                    array[i] = temp;
                    isSorted = false;
                }
            }
        }
        long stopTime = System.nanoTime();
        System.out.println((double)(stopTime - startTime) / 1000000000);
    }
    
    //Сортировка вставками
    public static void insertionSort(int[] array) {
        long startTime = System.nanoTime();
        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            int j = i - 1;
            while (j >= 0 && current < array[j]) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = current;
        }
        long stopTime = System.nanoTime();
        System.out.println((double)(stopTime - startTime) / 1000000000);
    }
    
    //сортировка выбором
    public static void selectionSort(int[] array) {
        long startTime = System.nanoTime();
    
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minId = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minId = i;
                }
            }
            int temp = array[i];
            array[i] = min;
            array[minId] = temp;
        }
        long stopTime = System.nanoTime();
        System.out.println((double)(stopTime - startTime) / 1000000000);
    }
    
    public static void radixSort(int[] array) {
        long startTime = System.nanoTime();
        int max = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        
        int[] counters = new int[max + 1];
        for (int i = 0; i < array.length; i++) {
            counters[array[i]] += 1;
        }
        
        int idx = 0;
        for (int i = 0; i < counters.length; i++) {
            for (int j = 0; j < counters[i]; j++) {
                array[idx] = i;
                idx++;
            }
        }
        long stopTime = System.nanoTime();
        System.out.println((double)(stopTime - startTime) / 1000000000);
    }
}
