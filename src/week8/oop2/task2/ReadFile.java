package week8.oop2.task2;

import java.io.*;
import java.util.Scanner;

public class ReadFile {
    
    private static final String FOLDER_DIRECTORY = "D:\\Рабочий стол\\Учу java\\Java13Basic\\src\\week8\\oop2\\task2\\file";
    
    private static final String OUTPUT_FILE_NAME = "output.txt";
    
    private ReadFile() {
    }
    
    public static void readDataFromFile(String filePath) throws IOException { //В инфа о том, что может выбросить исключение
        Scanner sc = new Scanner(new File(filePath));
        //Scanner sc = new Scanner(new File(FOLDER_DIRECTORY + "\\input.txt"));
        String[] days = new String[10];
        int i = 0;
        while (sc.hasNextLine()) {
            days[i++] = sc.nextLine();
        }
    
        Writer writer = new FileWriter(FOLDER_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        for (int j = 0; j < i; j++) {
            String res = "Порядковый номер дня недели " + days[j] + " = " + WeekDays.ofName(days[j]).dayNumber + "\n";
            writer.write(res);
        }
//        //Пример try with resources (Closable интерфейс) -> не надо явно закрывать потоки (ресурсы)
//        try (Writer writer1 = new FileWriter("")) {
//            System.out.println();
//        }
        
        writer.close();
        sc.close();
    }
    
    public static void readDataFromFile() throws IOException { //Перегруженный метод, для пользователя, который не знает путь, делаем по умолчанию
        readDataFromFile(FOLDER_DIRECTORY + "\\input.txt");
    }
}
