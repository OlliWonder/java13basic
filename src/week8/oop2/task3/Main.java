package week8.oop2.task3;

public class Main {
    public static void main(String[] args) {
        SimpleArrayList myList = new SimpleArrayList();
        myList.add(12);
        myList.add(13);
        myList.add(15);
        myList.add(16);
        myList.add(17);
        myList.add(18);
        System.out.println("ArrayList size: " + myList.size());
        System.out.println("Элемент с первым индексом: " + myList.get(0));
        System.out.println("Элемент со вторым индексом: " + myList.get(1));
        System.out.println("Элемент с последним индексом: " + myList.get(myList.size() - 1));
        
    }
}
