package week8.oop2.task3;

import java.security.PublicKey;
import java.util.Arrays;

/* Примитивная реализация ArrayList.
Массив только int, из методов только добавлять элемент, получать size
и увеличивать капэсити (ёмкость или вместимость), когда добавляетсяновый.
 */
public class SimpleArrayList {
    // Количество элементов
    private int size;
    private int[] arr;
    // Объем массива
    private int capacity;
    private static final int DEFAULT_CAPACITY = 5;
    private int currentIndex;
    
    public SimpleArrayList() {
        arr = new int[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
        currentIndex = 0;
    }
    
    public SimpleArrayList(int size) {
        arr = new int[size];
        capacity = size;
        this.size = 0;
        currentIndex = 0;
    }
    
    /**
     * Добавляем новый элемент в список(массив). При достижении размера внутреннего массива
     * происходит его увеличение в два раза.
     */
    public void add(int elem) {
        if (currentIndex >= capacity) {
            capacity = 2 * capacity;
            arr = Arrays.copyOf(arr,capacity);
            //System.arraycopy() - тоже может скопировать
        }
        arr[currentIndex] = elem;
        size++;
        currentIndex++;
    }
    
    public void remove(int idx) {
        for (int i = idx; i < currentIndex; i++) {
            arr[i] = arr[i + 1];
        }
        arr[currentIndex] = -1;
        currentIndex--;
        size--;
    }
    
    /**
     * Возможная реализвция метода удаления из MyArrayList
     */
    public int get(int idx) {
        if (idx < 0 || idx >= size) {
            // или выбросить исключение
            System.out.println("Неправильный индекс: " + idx);
            return -1;
        }
        else {
            return arr[idx];
        }
    }
    
    public int size() {
        return size;
    }
}
