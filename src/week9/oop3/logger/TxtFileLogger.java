package week9.oop3.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class TxtFileLogger
        extends FileNameHandler
        implements Logger {
    private final String fileName;
    
    public TxtFileLogger() {
        this.fileName = getDefaultFileName();
    }
    
    public TxtFileLogger(String fileName) {
        this.fileName = fileName;
    }
    
    @Override
    public void log(String message) {
        try (Writer writer = new FileWriter(fileName + getExtension(), true)) {
            writer.write(message + "\n");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public String getExtension() {
        return FileExtentions.TXT_FILE_EXTENTION;
    }
}
