package week9.oop3.logger;

public class Bulb {
    private boolean toggle;
    private Logger logger;
    
    public Bulb(Logger logger) {
        this.logger = logger;
    }
    
    public void turnOn() {
        toggle = true;
        logger.log("Лампочка включена");
    }
    
    public void turnOff() {
        toggle = false;
        logger.log("Лампочка выключена");
    }
    
    public boolean isShining() {
        logger.log("Получили информацию о состоянии лампочки");
        return toggle;
    }
}
