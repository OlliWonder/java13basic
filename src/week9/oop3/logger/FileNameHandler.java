package week9.oop3.logger;

public abstract class FileNameHandler {
    private String defaultFileName = "default_output";
    
    public String getDefaultFileName() {
        return defaultFileName;
    }
    
    public abstract String getExtension();
}
