package week9.oop3.logger;

import java.io.Serializable;

public interface Logger extends Serializable {
    
    void log(String message);
    
    
}
