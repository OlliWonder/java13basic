package week9.oop3.logger;

public final class ConsoleLogger
    implements Logger {
    
    @Override
    public void log(String message) {
        System.out.println("[ConsoleLogger]#log(): " + message);
    }
}

