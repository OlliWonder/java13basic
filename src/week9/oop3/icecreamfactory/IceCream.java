package week9.oop3.icecreamfactory;

//https://refactoring.guru/ru/design-patterns/factory-method
//Ресурс по паттернам самый лучший (ИМХО)
//VPN ONLY

public interface IceCream {
    void printIngredients();
}
