package week9.oop3.icecreamfactory;

public class VanillaIceCream
        implements IceCream {
    @Override
    public void printIngredients() {
        System.out.println("Vanilla, Cream, Ice, Love, Stevia");
    }
}