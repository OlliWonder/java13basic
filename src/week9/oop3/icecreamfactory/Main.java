package week9.oop3.icecreamfactory;

public class Main {
    public static void main(String[] args) {
        IceCream cherryIceCream = IceCreamFactory.getIceCream(IceCreamType.CHERRY);
        IceCream vanilla = IceCreamFactory.getIceCream(VanillaIceCream.class);
        cherryIceCream.printIngredients();
        vanilla.printIngredients();
    }
}
