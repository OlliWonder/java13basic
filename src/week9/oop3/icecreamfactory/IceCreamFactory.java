package week9.oop3.icecreamfactory;

/**
 * Реализация паттерна Фабрика для нашего мороженого. Фабрика, как паттерн является самой простой.
 */

public class IceCreamFactory {
    
    private IceCreamFactory() {
    }
    
    public static IceCream getIceCream(IceCreamType type) {
        IceCream iceCream = null;
        switch (type) {
            case CHERRY -> iceCream = new CherryIceCream();
            case CHOCOLATE -> iceCream = new ChocolateIceCream();
            case VANILLA -> iceCream = new VanillaIceCream();
        }
        return iceCream;
    }
    
//    public static IceCream getIceCream(Object o) {
//        IceCream iceCream = null;
//        if (o instanceof ChokolateIceCream) {
//            iceCream = new ChocolateIceCream();
//        }
//    } // ТАК НЕ НАДО!!
    
    public static IceCream getIceCream(Class obj) {
        System.out.println("OBJECT: " + obj);
        IceCream iceCream = null;
        
        if (obj.equals(ChocolateIceCream.class)) {
            iceCream = new ChocolateIceCream();
        }
        if (obj.equals(CherryIceCream.class)) {
            iceCream = new CherryIceCream();
        }
        if (obj.equals(VanillaIceCream.class)) {
            iceCream = new VanillaIceCream();
        }
        return iceCream;
    }
}
