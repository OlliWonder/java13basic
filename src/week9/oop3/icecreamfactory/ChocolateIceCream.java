package week9.oop3.icecreamfactory;

public class ChocolateIceCream
        implements IceCream {
    @Override
    public void printIngredients() {
        System.out.println("Chocolate, Cream, Ice");
    }
}