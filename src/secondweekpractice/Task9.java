package secondweekpractice;
import java.util.Scanner;
/*
   Дана строка и паттерн, заменить паттерн на паттерн, состоящий из заглавных символов
   Входные данные
   Hello
   o
   Выходные данные
   HellO

   Входные данные
   Hello world
   ld
   Выходные данные
   Hello worLD
    */

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String a = input.nextLine();
        String b = input.next();

        String upperPattern = b.toUpperCase();
        System.out.println(a.replace(b, upperPattern));
    }
}
