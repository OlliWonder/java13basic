package secondweekpractice;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        System.out.println(n % 2 == 0 ? n + " - число чётное" : n + " - число нечётное");
    }
}
