package secondweekpractice;

import java.util.Scanner;
/*
Дан символ, поменять со строчного на заглавный или с заглавного на строчный

Входные данные
d
Выходные данные
D
Входные данные
A
Выходные данные
a
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.next();
        char ch = s.charAt(0);
        System.out.println((ch >= 'A' && ch <= 'Z') ? Character.toLowerCase(ch) : Character.toUpperCase(ch));
        if (ch >= 'a' && ch <= 'z'){ // Второй способ
            System.out.println((char) (ch - ('a' - 'A')));
        } else {
            System.out.println((char) (ch + ('a' - 'A')));
        }
    }
}