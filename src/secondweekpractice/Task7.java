package secondweekpractice;

import java.util.Scanner;
/*
Реализовать System.out.println(), используя System.out.print() и перенос \n
Входные данные: два слова, считываемые из консоли

Входные данные
Hello World
Выходные данные
Hello
World

 */

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String a = input.next();
        String b = input.next();
        System.out.print(a + "\n" + b);
    }
}