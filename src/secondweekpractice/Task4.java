package secondweekpractice;

import java.util.Scanner;
/*
 Считать данные из консоли о типе номера отеля.
 1 VIP, 2 Premium, 3 Standard
 Вывести цену номера VIP = 125, Premium = 110, Standard = 100
 */
public class Task4 {
    public static final int VIP_PRICE = 125;
    public static final int PREMIUM_PRICE = 110;
    public static final int STANDART_PRICE = 100;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int roomType = input.nextInt();
        if (roomType == 1) {
            System.out.println("номер Вип стоит " + VIP_PRICE);
        }
        else if (roomType == 2) {
            System.out.println("Номер Премиум стоит " + PREMIUM_PRICE);
        }
        else if (roomType == 3) {
            System.out.println("Номер стандарт стоит " + STANDART_PRICE);
        }
        else {
            System.out.println("Введите корректный номер"); //почему-то всегда печатает этоо
       /* switch (roomType) {
            case 1:
                System.out.println(VIP_PRICE);
                break;
            case 2:
                System.out.println(PREMIUM_PRICE);
                break;
            case 3:
                System.out.println(STANDART_PRICE);
                break;
            default:
                System.out.print("введите корректный номер");
                break; */
                /*    switch (roomType) { // Новая версия свитча
            case 1 -> System.out.println(VIP_PRICE); // лямбда - выражение
            case 2 -> System.out.println(PREMIUM_PRICE);
            case 3 -> System.out.println(STANDART_PRICE);
            default -> System.out.print("введите корректный номер"); */
        }
    }
}