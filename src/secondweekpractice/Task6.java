package secondweekpractice;

import java.util.Scanner;
/*
Даны три числа a, b, c.
Найти сумму двух чисел больших из них.
Входные данные
21 0 8
Выходные данные
29
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c= input.nextInt();

        int sum;

        System.out.println(Math.max(Math.max(a + b, b + c), a + c));
    }
}

