package week7.oop1.task3;

public class Main {
    public static void main(String[] args) {
        System.out.println(FileValidator.validateEmail("pure_sunlight13@yandex.com"));
        System.out.println(FileValidator.validateDate("22.04.1991"));
        System.out.println(FileValidator.validateName("Olga"));
        System.out.println(FileValidator.validatePhone("+79180403841"));
    }
}
