package week7.oop1.task1;

import java.sql.SQLOutput;

public class Main {
    public static void main(String[] args) {
        Bulb bulb = new Bulb(true);
        System.out.println("Светит ли лампа? " + bulb.isShining());
        bulb.turnOff();
        System.out.println("Светит ли лампа? " + bulb.isShining());
        
        Chandelier chandelier = new Chandelier(4);
        chandelier.turnOn();
        chandelier.turnOff();
    }
}
