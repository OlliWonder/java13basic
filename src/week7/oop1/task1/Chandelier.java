package week7.oop1.task1;
//Включать и выключать люстру и показывать её состояние
public class Chandelier {
    private Bulb[] chandelier;
    //Конструктор люстры
    public Chandelier(int countOfBulbs) { //количество ламп в люстре
        chandelier = new Bulb[countOfBulbs];
        //Заполняем лампами
        for (int i = 0; i < countOfBulbs; i++) {
            chandelier[i] = new Bulb();
        }
    }
    //включение ламп люстре
    public void turnOn() {
        for (Bulb bulb : chandelier) {
            bulb.turnOn();
        }
    }
    //выключение
    public void turnOff() {
        for (Bulb bulb : chandelier) {
            bulb.turnOff();
        }
    }
    
    public boolean isShining() {
        return chandelier[0].isShining();
    }
}
