package fifthweekpraktice;
/*
На вход подается число N — длина массива.
Затем передается массив целых чисел длины N.

Проверить, является ли он отсортированным массивом строго по убыванию.
Если да, вывести true, иначе вывести false.

5
5 4 3 2 1
->
true

2
43 46
->
false

3
5 5 5
->
false

 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
//        for (int i = 0; i < n - 1; i++) {
//            if (arr[i] > arr[i + 1]) {
//            }
//            else {
//                System.out.println(false);
//                return;
//            }
//        }
//        System.out.println(true);
        System.out.println(check(arr));
    }
    public static boolean check(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] <= arr[i + 1]) {
                return false;
            }
        }
        return true;
    }
}