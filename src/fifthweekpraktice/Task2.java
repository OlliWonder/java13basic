package fifthweekpraktice;
/*
На вход подается число N — длина массива.
Затем передается массив целых чисел длины N.

Вывести элементы, стоящие на четных индексах массива.
4
20 20 11 13
->
20 11
 */
import java.util.Scanner;
public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                System.out.println(arr[i]);
            }
        }
    }
}
