package fifthweekpraktice;
/*
     На вход подается число N - длина массива.
     Затем подается массив целых чисел из N элементов.

     Нужно циклически сдвинуть элементы на 1 влево.

     Входные данные:
     5
     1 2 3 4 7
     Выходные данные:
     2 3 4 7 1
     */
import java.util.Scanner;
import java.util.Arrays;
public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        int temp = arr[0];
        for (int i = 1; i < arr.length; i++) {
            arr[i - 1] = arr[i];
        }
        arr[arr.length - 1] = temp;

        for (int e : arr) {
            System.out.print(e + " ");
        }

        int first = arr[0];
        System.arraycopy(arr, 1, arr, 0, arr.length - 1);
        arr[arr.length - 1] = first;
        System.out.println(Arrays.toString(arr));
    }
}
