package thirdweekpractice;
/*
Перевод и КамелКейс в снэйк_кейс
На вход подается строка, состоящая из заглавных и прописных латинских букв (вида кэмел).
Вывести эту же строку, но состоящую только из прописных букв (снейк).
А перед местом второй и последующей заглавной вывести _.

ItIsCamelCaseString
it_is_camel_case_string

SomeLongVariable
some_long_variable
 */
import java.util.Scanner;
public class Task4 {
    public static void main(String[] args) {
        String input = new Scanner(System.in).nextLine();
        String output = input.replaceAll("([a-z])([A-Z])", "$1_$2").toLowerCase();

        System.out.println("начальная строка: " + input);
        System.out.println("обновленная строка: " + output);
    }
}