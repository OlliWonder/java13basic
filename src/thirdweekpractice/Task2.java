package thirdweekpractice;
/* Проверить номер карты и пин-код
На вход подаётся две строки: первая содержит номер карты, вторая - пин-код
Проверить, что первая состоит из 16 цифр, разделенных пробелом,
и что вторая состоит из 4 цифр.
 */
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String cardNumber = input.nextLine();
        String code = input.nextLine();

        System.out.println("Card number is valid: " + cardNumber.matches("[1-9][0-9]{3}( \\d{4}){3}"));
        // "[1-9][0-9]{3} \\d{4} \\d{4} \\d{4}" - моё первое решение
        System.out.println("Pin is valid: " + code.matches("\\d{4}"));
    }
}
