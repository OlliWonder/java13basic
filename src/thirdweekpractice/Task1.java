package thirdweekpractice;
/*
Проверить, является ли введенная строка корректным hex номером цветаю. (hex - шестнадцатиричным)/
Корректная строка состоит из 7 символов, первый символ # , далее цифры или буквы от A до F (заглавные или прописные)
Ясли строка корректна - вывести true, иначе - false.

#00AA12
true

00FFFF
false
 */
import java.util.Scanner;
public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // String color = new Scanner(System.in).next();
        String color = input.next();
        boolean res = color.matches("#[0-9A-Fa-f]{6}"); // System.out.println(color.matches(regex));
        System.out.println(res);
    }
}
