package thirdweekpractice;
/*
Запросить у пользователя имя, день рождения, номер телефона, email.
Каждое из полученных ответов проверить регулярным выражением по описанным ниже правилам.
Если все введено верно: вывести Ок.
Если хотя бы одно не совпадает: вывести Wrong Answer и завершить работу.
Проверки:
Имя - Должно содержать только буквы, навчинаться с заглавной и далее только прописные. От 2 до 20 символов.

ДР - имеет вид дд.мм.гггг цифры без ограничений.

Номер телефона
Должен начинаться со знака + и далее ровно 11 цифр.

Email: В начале идут прописные буквы или цифрыы и может быть один из символов _-*
Далее обязательно @
Далее прописные буквы или цифры. Точка. com или ru.

 */
import java.util.Scanner;
public class Task3 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        String date = input.nextLine();
        String phone = input.nextLine();
        String email = input.nextLine();

        boolean nm = name.matches("[A-Z][a-z]{1,19}");
        boolean dt = date.matches("[0-3][0-9]\\.[01][0-9]\\.\\d{4}");
        boolean pn = phone.matches("\\+[0-9]{11}");
        boolean em = email.matches(".*[a-z0-9_*-]@*[a-z0-9]\\.(com|ru)");
        System.out.println(nm && dt && pn && em ? "Ok" : "Wrong Answer");
    }
}
