package fourthweekpractice;
/*
На вход подается число n и последовательность целых чисел длины n.
Вывести два максимальных числа в этой последовательности без использования массивов.
5
1 3 5 4 5 -> 5 5
 */
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        int firstNumber = input.nextInt();
        int secondNumber = input.nextInt();

        int firstMax = Math.max(firstNumber, secondNumber);
        int secondMax = Math.min(firstNumber, secondNumber);

        for (int i = 2; i < n; i++) {
            int nextNumber = input.nextInt();

            if (nextNumber > firstMax) {
                secondMax = firstMax;
                firstMax = nextNumber;
            } else if (nextNumber > secondMax) {
                secondMax = nextNumber;
            }
        }
        System.out.println(firstMax + "" + secondMax);
    }
}
