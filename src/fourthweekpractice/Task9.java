package fourthweekpractice;
/*
 Дана последовательность из n целых чисел, которая может начинаться с
    отрицательного числа. Определить, какое количество отрицательных чисел
    записано в начале последовательности и прекратить выполнение программы
    при получении первого неотрицательного числа на вход.
Входные данные:
-1
-2
4
Выходные данные:
Result: 2
 */

import java.util.Scanner;
public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int count = 0;
        for (int i = input.nextInt(); i < 0; i = input.nextInt()) {
            count++;
        }
        System.out.println(count);
//       1 case:
//        Scanner scanner = new Scanner(System.in);
//        int result = 0;
//        while (true) {
//            if (scanner.nextInt() < 0)
//                result++;
//            else
//                break;
//        }
//        System.out.println(result);

    }
}