package fourthweekpractice;
/*
Дано целое число n, n > 0. Вывести сумму всех цифр этого числа.
92180 -> 20 //9 + 2 + 1 + 8 + 0 == 20
 */

import java.util.Scanner;
public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            n /= 10;
        }
        System.out.println(sum);
    }
}
