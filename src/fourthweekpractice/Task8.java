package fourthweekpractice;
// Дана строка s.
// Вычислить количество символов в ней, не считая пробелов (необходимо использовать цикл).
import java.util.Scanner;
public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        int result = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ') {
                result++;
            }
        }
        System.out.println(result);
    }
}
