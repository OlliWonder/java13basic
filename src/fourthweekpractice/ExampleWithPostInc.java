package fourthweekpractice;

public class ExampleWithPostInc {
    public static void main(String[] args) {
//        int i = 1;
//        int a = i++;
//        int temp = i;
//        i = i + 1;
//        a = temp;

//        int i = 1;
//        int a = ++i;
        int i = 1;
        int a = (i++) + (++i);
        System.out.println("i " + i);
        System.out.println("a " + a);
    }
}
