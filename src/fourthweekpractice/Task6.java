package fourthweekpractice;
/*
Дано целое число n.
Найти n число Фибоначчи с помощью цикла.
//0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946
F0 = 0
F1 = 1
Fn = Fn-1 + Fn-2
Фибоначчи - последовательность, в которой первые два числа равны 0 и 1,
а каждое последующее число равно сумме двух предыдущих чисел
Fn = Fn-1 + Fn-2, n>=2

 */
import java.util.Scanner;
public class Task6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        int fb0 = 0;
        int fb1 = 1;

        if (n == 0) {
            System.out.println(fb0);
            System.exit(0);
        }
        if (n == 1) {
            System.out.println(fb1);
            return; // использовать предпочтительнее, чем систем экзит
        }
        int fbRes = 0;
        for (int i = 2; i <= n; i++) {
            fbRes = fb0 + fb1;
            fb0 = fb1;
            fb1 = fbRes;
        }
        System.out.println(fbRes);
    }
}
