package fourthweekpractice;
/*
Дано число n < 13, n > 0. Найти факториал числа n (n! = 1 * 2 * 3 * … * (n - 1) * n)
7 -> 5040
 */
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int fact = 1;
        for (int i = 2; i <= n; i++) {
            fact *= i;
        }
        System.out.println(fact);
    }
}
/* int res = 1;
  for (int i = 1; i <= n ; i++,res*=i) {
      System.out.println(res);
          } */
