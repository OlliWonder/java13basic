package sixweekpractice;
/*
Развернуть строку рекурсивно.

abcde -> edcba
 */
import java.util.Scanner;
public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        System.out.println(reversString(s));
    }
    
    public static String reversString(String s) {
        if (s.length() == 0 || s.length() == 1) {
            return s;
        }
        else {
            return s.charAt(s.length() - 1) + reversString(s.substring(0, s.length() - 1));
        }
    }
}
