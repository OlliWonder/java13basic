package sixweekpractice;
/*
Найдем факториал числа n рекурсивно.
 */

import java.util.Scanner;
public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        
//        int res = 1;
//        for (int i = 1; i <= n ; i++) {
//            res *= i;
//        }
        System.out.println("Factorial: " + factorial(n));
    }
    
    public static int factorial(int n) {
        if (n <= 1) {
            return 1;
        }
        return n * factorial(n - 1);
    }
    
//    res = factorial(n,1);
//        System.out.println(res);
//}
//
//    public static int factorial(int n, int result){
//        if(n<=1)
//            return result;
//        else
//            return factorial(n-1,n*result);
//    }

}
