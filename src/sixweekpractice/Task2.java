package sixweekpractice;
/*
На вход подается натуральное число N.
Необходимо проверить, является ли оно степенью двойки (решить через рекурсию).
Вывести true, если является и false иначе.

4 -> true
5 -> false
6 -> false
7 -> false
8 -> true
 */
import java.util.Scanner;
public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        System.out.println(checkPowerOfTwo(n));
    }
    
    public static boolean checkPowerOfTwo(int n) {
        if (n % 2 != 0) {
            return false;
        }
        else if (n / 2 == 1) {
            return true;
        }
        else {
            return checkPowerOfTwo(n / 2);
        }
    }
    
//    public static boolean checkPowerOfTwo(long n) {
//        if (n == 2 || n == 1) {
//            return true;
//        }
//        if (n <= 0 || n % 2 != 0) {
//            return false;
//        }
//        return checkPowerOfTwo(n / 2);
//    }
}
