package pro.week2.task5;

import java.util.Collections;
import java.util.HashSet;
import java.util.TreeSet;

//Создать метод, переводящий из HashSet в TreeSet. Вывести оба варианта.
public class ConvertHashSet {
    private ConvertHashSet() {
    }
    
    public static <T> TreeSet<T> convertHashSet(HashSet<T> from) {
        TreeSet<T> toReturn = new TreeSet<>();
        //Collections.addAll();
        
        //toReturn.addAll(from);
        
        for (T elem : from) {
            toReturn.add(elem);
        }
        return toReturn;
    }
}
