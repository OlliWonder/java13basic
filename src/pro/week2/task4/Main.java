package pro.week2.task4;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Boolean> list1 = new ArrayList<>();
        list1.add(true);
        list1.add(true);
        list1.add(false);
        System.out.println(ListUtil.countIf(list1, true));
        System.out.println(ListUtil.countIf(list1, false));
        
        List<String> list2 = new ArrayList<>();
        list2.add("abc");
        list2.add("qqq");
        list2.add("qwerty");
        list2.add("qwerty");
        System.out.println(ListUtil.countIf(list2, "qwerty"));
        System.out.println(ListUtil.countIf(list2, new String("qwerty")));
        
        //говорим про string pool
        String str1 = "TopJava"; //строка создана из строкового литерала
        String str2 = "Top" + "Java"; //конкатенация известна на этапе компиляции
        String str3 = "Top" + str2; //конкатенация НЕизвестна на этапе компиляции
    
        System.out.println("Строка 1 равна строке 2? " + (str1 == str2));
    
        String st1 = "TopJava";
        String st2 = "TopJava";
        String st3 = (new String("TopJava")).intern();
        String st4 = (new String("TopJava")).intern();
    
        System.out.println("Строка 1 равна строке 2? " + (st1 == st2));
        //https://topjava.ru/blog/rukovodstvo-po-string-pool-v-java
    
    }
}
