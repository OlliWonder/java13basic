package pro.week2.task3;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/*
 На вход подаются два сета, вывести уникальные элементы,
 которые встречаются и в первом и во втором.
 */
public class Sets {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);
        System.out.println(set1);
    
        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(3);
        set2.add(2);
        set2.add(2);
        System.out.println(set2);
        
        //встроенный метод выводит пересечение множеств
        set1.retainAll(set2);

        for (Integer e : set1) {
            System.out.println(e);
        }
        //два варианта, но короче
        set1.forEach(e -> System.out.println(e));
        set1.forEach(System.out::println);
        
        //метод из библиотеки Collections: вернёт false, если есть хоть один элемент
        // в нашей текущей коллекции от другой коллекции. true - если ничего не совпало.
        System.out.println(Collections.disjoint(set1, set2));
    }
}
