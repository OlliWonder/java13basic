package pro.week2.task1;

public class PairC<T extends String, U extends Number> {
    public T first;
    public U second;
    
    public void print() {
        System.out.println("First: " + first + "; Second: " + second);
    }
}
