package pro.week2.task1;

public class PairB<T> {
    public T first;
    public T second;
    
    public void print() {
        System.out.println("First: " + first + "; Second: " + second);
    }
}
