package pro.week2.task1;

import java.math.BigDecimal;

/*
Создать класс Pair, который умеет хранить два значения:
a) любого типа
b) одинакового типа
c) первый только String, второй только числовой
 */
public class Main {
    public static void main(String[] args) {
        Pair<String, Integer> pair = new Pair<>();
        pair.first = "Test first field";
        pair.second = 123;
        pair.print();
        
        PairC<String, BigDecimal> pairC = new PairC<>();
        pairC.first = "123";
        pairC.second = new BigDecimal(123);
        pairC.print();
    }
}
