package pro.week2.task1;

public class Pair<T, U> {
    public T first;
    public U second;
    
    public void print() {
        System.out.println("First: " + first + "; Second: " + second);
    }
}
