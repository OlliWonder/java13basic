package pro.week2.task2;

public class Pair<T, U> {
    private T first;
    private U second;
    
    public T getFirst() {
        return first;
    }
    
    public void setFirst(T first) {
        this.first = first;
    }
    
    public U getSecond() {
        return second;
    }
    
    public void setSecond(U second) {
        this.second = second;
    }
    
    public void print() {
        System.out.println("First: " + first + "; Second: " + second);
    }
    
    @Override
    public String toString() {
        return "Pair{" +
                "first=" + first +
                ", second=" + second +
                '}'; //формат похож на json
    }
}
