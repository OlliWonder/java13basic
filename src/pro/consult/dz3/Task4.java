package pro.consult.dz3;

import java.util.ArrayList;
import java.util.List;

interface A {}

interface B {}

interface C {}

interface D
        extends A, B {}

interface E
        extends C, D {}

class Cl1
        implements A {}

class Cl2
        extends Cl1
        implements E {}

public class Task4 {
    public static void main(String[] args) {
        List<Class<?>> result = getAllInterfaces1(Cl2.class);
        for (Class<?> anInterface : result) {
            System.out.println(anInterface.getName());
        }
    }
    
    public static List<Class<?>> getAllInterfaces1(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        else {
            List<Class<?>> interfaces = new ArrayList<>();
            getAllInterfacesOfParents(cls, interfaces);
            return interfaces;
        }
    }
    
    public static void getAllInterfacesOfParents(Class<?> cls, List<Class<?>> interfacesFound) {
        while (cls != null) {
            Class<?>[] interfaces = cls.getInterfaces();
            for (Class<?> anInterface : interfaces) {
                if (!interfacesFound.contains(anInterface)) {
                    interfacesFound.add(anInterface);
                    getAllInterfacesOfParents(anInterface, interfacesFound);
                }
            }
            cls = cls.getSuperclass();
        }
    }
}
