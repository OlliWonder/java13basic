package pro.consult.dz1.task1;

public class Task1 {
    public static void main(String[] args) {
        try {
            method(0);
            method(1);
        }
        catch (MyChecedException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void method(int i) throws MyChecedException {
        if (i == 0) {
            System.out.println("0");
        }
        else {
            throw new MyChecedException("not a zero");
        }
    }
}
