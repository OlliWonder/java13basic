package pro.consult.dz1.task1;

public class MyChecedException extends Exception {
    
    public MyChecedException(String s) {
        super(s);
    }
}
