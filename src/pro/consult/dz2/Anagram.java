package pro.consult.dz2;

import java.util.Arrays;
import java.util.HashMap;

public class Anagram {
    public static void main(String[] args) {
    
    }
    
    public static boolean isAnagram(String s, String t) {
        return Arrays.equals(s.chars().sorted().toArray(), t.chars().sorted().toArray()); //через стримы
    }
    
    public static boolean isAnagram1(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }
        HashMap<Character, Integer> s1 = new HashMap<>();
        for (char element : str1.toLowerCase().toCharArray()) {
            s1.put(element, s1.getOrDefault(element, 0) + 1);
        }
        
        HashMap<Character, Integer> s2 = new HashMap<>();
        for (char element : str2.toLowerCase().toCharArray()) {
            s2.put(element, s2.getOrDefault(element, 0) + 1);
        }
        return s1.equals(s2);
    }
}
