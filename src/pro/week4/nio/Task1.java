package pro.week4.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
//создаем файл
public class Task1 {
    public static void main(String[] args) {
        Path path = Paths.get("src\\test.txt");
        try {
            Path createdFilePath = Files.createFile(path);
        }
        catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }
}
