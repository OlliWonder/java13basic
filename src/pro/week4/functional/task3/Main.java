package pro.week4.functional.task3;

public class Main {
    public static void main(String[] args) {
        Pi pi = () -> Math.PI;
        System.out.println(pi.getPi());
    
        Pi pi2 = () -> 3.1415;
        System.out.println(pi2.getPi());
    }
}
