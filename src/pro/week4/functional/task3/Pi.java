package pro.week4.functional.task3;
@FunctionalInterface
public interface Pi {
    double getPi();
}
