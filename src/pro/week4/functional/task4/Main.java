package pro.week4.functional.task4;
/*
реализовать метод, чтобы вывести строку наоборот, используя наш ReverseInterface
 */
public class Main {
    public static void main(String[] args) {
        MyReverseInterface myReverseInterface = s -> new StringBuilder(s).reverse().toString();
        System.out.println(myReverseInterface.reversString("Hello"));
        
        MyReverseInterface myReverseInterface1 = s -> {
            String res = "";
            for (int i = s.length() - 1; i >= 0 ; i--) {
                res += s.charAt(i);
            }
            return res;
        };
        System.out.println(myReverseInterface1.reversString("World"));
    }
}
