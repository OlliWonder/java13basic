package pro.week4.functional.task4;
@FunctionalInterface
public interface MyReverseInterface {
    String reversString(String s);
}
