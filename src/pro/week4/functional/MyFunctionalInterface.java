package pro.week4.functional;
@FunctionalInterface
public interface MyFunctionalInterface {
    //Single Abstract Methods (SAM)
    double getValue();
}
