package pro.week4.functional.task5;
/*
Создать параметризованный функциональный интерфейс (Generics)
На выходе получить:
1) Строку наоборот
2) Факториал числа
 */
public class Main {
    public static void main(String[] args) {
        //reverse
        MyMagicInterface<String> result = s -> new StringBuilder(s).reverse().toString();
        System.out.println(result.execute("Hello"));
        
        MyMagicInterface<Integer> factorial = n -> {
            int fact = 1;
            for (int i = 1; i <= n ; i++) {
                fact *= i;
            }
            return fact;
        };
        System.out.println(factorial.execute(4));
    }
}
