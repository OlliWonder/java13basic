package pro.week4.functional.task2;
/*
Задача 2
С помощью функционального интерфейса выполнить подсчет квадрата числа:
 */
public class Main {
    public static void main(String[] args) {
        Square square = new Square() { // с помощью анонимного класса
            @Override
            public int calculate(int x) {
                return x * x;
            }
        };
        
        int value = 3;
        System.out.println(square.calculate(value));
        
        Square square1 = x -> x * x; // лямбда
        //или  Square square1 = (int x) -> x * x;
        System.out.println(square1.calculate(3));
        
    }
}
