package pro.week4.streams;

import java.util.List;

public class StreamsExample {
    public static void main(String[] args) {
        List<String> places = List.of("Nepal, Kathmandu", "Nepal, Pokhara", "India, Delhi", "USA, New York", "Africa, Nigeria"
        );
        places.stream()
                .filter(place -> place.startsWith("Nepal"))
                //.map(place -> place.toUpperCase())
                .map(String::toUpperCase)
                .sorted()
                //.forEach(place -> System.out.println(place))
                .forEach(System.out::println);
    }
}
