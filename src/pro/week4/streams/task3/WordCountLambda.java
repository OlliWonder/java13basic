package pro.week4.streams.task3;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WordCountLambda {
    public static void main(String[] args) {
        List<String> names = List.of("Sam", "James", "Elena", "James", "Joe", "Sam", "James");
        
        //1 способ
        Set<String> unique = new HashSet<>(names);
        for (String key : unique) {
            System.out.println(key + ": " + Collections.frequency(names, key));
        }
        
        //2 способ
        Map<String, Long> frequencyMap = names.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting())); //группировка по сущности и число
        System.out.println(frequencyMap);
        /*
        SQL - запрос
        SELECT name, count(*) FROM names GROUP BY name
         */
        
        //3 способ
        Map<String, Integer> counts = names.parallelStream()//или просто stream
                .collect(Collectors.toConcurrentMap(v -> v, v -> 1, Integer::sum));
        System.out.println(counts);
    }
}
