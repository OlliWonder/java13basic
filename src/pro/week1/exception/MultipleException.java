package pro.week1.exception;

public class MultipleException {
    public static void main(String[] args) {
        try {
            someThrowArrayIndexOutOfBoundsException();
            toDivideThrowMyArithmeticException(100, 0);
            simpleThrowRuntimeException();
        }
        catch (RuntimeException | MyArithmeticException e) {
            System.out.println("LOG: произошло исключение! " + e.getMessage());
        }
//        catch (ArrayIndexOutOfBoundsException e) {
//            System.out.println("LOG: произошло исключение! ArrayIndexOutOfBoundsException");
//        }
//        catch (RuntimeException e) {
//            System.out.println("LOG: произошло исключение! RuntimeException");
//        }
//        catch (MyArithmeticException e) {
//            System.out.println("LOG: произошло исключение! MyArithmeticException");
//        }
    }
    
    public static void simpleThrowRuntimeException() {
        throw new RuntimeException();
    }
    
    public static void someThrowArrayIndexOutOfBoundsException() {
        int[] arr = new int[10];
        System.out.println(arr[10]);
    }
    
    public static void toDivideThrowMyArithmeticException(int a, int b)
            throws MyArithmeticException {
        try {
            System.out.println(a / b);
        }
        catch (ArithmeticException e) {
            throw new MyArithmeticException();
        }
    }
}
