package pro.week1.exception;

public class MyArithmeticException
        extends Exception {
    
    public MyArithmeticException() {
        super("произошло деление на ноль");
    }
    
    public MyArithmeticException(String message) {
        super(message);
    }
}
