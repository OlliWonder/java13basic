package pro.week1.exception.task2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileOutput {
    private static final String PKG_DIRECTORY = "D:\\Рабочий стол\\Учу java\\Java13Basic\\src\\week8\\oop2\\task2\\file";
    
    private static final String OUTPUT_FILE_NAME = "output.txt";
    
    public static void main(String[] args) {
        try (Writer wr = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILE_NAME)) {
            wr.write("Hello!");
        }
        catch (IOException e) {
            System.out.println("FileOutput#main!error: " + e.getMessage());
        }
    }
}
