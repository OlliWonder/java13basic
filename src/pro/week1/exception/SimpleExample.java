package pro.week1.exception;

import java.util.Scanner;
//схема всех исключений в java
//https://javastudy.ru/wp-content/uploads/2016/01/exceptionsInJavaHierarchy.png
//stacktrace
public class SimpleExample {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

//        try {
//            toDivide(100, n);
//        }
//        catch (ArithmeticException e) {
//            throw new MyArithmeticException();
//        }
//        finally {
//            System.out.println("Программа завершена");
//        }
        try {
            toDivide(100, n);
        }
        catch (MyArithmeticException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //Более правильно (ниже), чтобы сам метод обрабатывал своё исключение.
    public static void toDivide(int a, int b) throws MyArithmeticException {
        try {
            System.out.println(a / b);
        }
        catch (ArithmeticException e) {
            throw new MyArithmeticException();
        }
        finally {
            System.out.println("Программа завершена");
        }
    }
}
