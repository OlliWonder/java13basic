package pro.week1.exception.task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

public class FileReadWrite {
    private static final String PKG_DIRECTORY = "D:\\Рабочий стол\\Учу java\\Java13Basic\\src\\week8\\oop2\\task2\\file";
    
    private static final String OUTPUT_FILE_NAME = "output.txt";
    private static final String INPUT_FILE_NAME = "input.txt";
    
    public static void main(String[] args) {
        try {
            readAndWriteFile();
        }
        catch (IOException e) {
            System.out.println("FileReadWrite#main!error: " + e.getMessage());
        }
    }
    
    public static void readAndWriteFile() throws IOException {
        Scanner scanner = new Scanner(new File(PKG_DIRECTORY + "\\" + INPUT_FILE_NAME));
        Writer writer = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        
        try (scanner; writer) {
            while (scanner.hasNext()) {
                writer.write(scanner.nextLine() + "\n");
            }
        }
    }
}
