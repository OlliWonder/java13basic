package pro.week3.annotations;

public class AnnotationReflection {
    public static void main(String[] args) {
    printClassDescription(MyPerfectClass.class);
    }
    public static void printClassDescription(Class<?> cls) {
        if (!cls.isAnnotationPresent(ClassDescription.class)) {
            return;
        }
        ClassDescription classDescription = cls.getAnnotation(ClassDescription.class);
        System.out.println("Автор: " + classDescription.author());
        System.out.println("Дата создания: " + classDescription.date());
        System.out.println("Текущий номер версии: " + classDescription.currentRevision());
        System.out.println("Список проверяющих:");
        for (String s : classDescription.reviewers()) {
            System.out.println(">>> " + s);
        }
    }
}
