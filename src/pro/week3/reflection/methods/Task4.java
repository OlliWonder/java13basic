package pro.week3.reflection.methods;

public class Task4 {
    int a;
    String b;
    
    @Override
    public String toString() {
        return "Task4{" +
                "a=" + a +
                ", b='" + b + '\'' +
                '}';
    }
    
    public Task4(int a, String b) {
        this.a = a;
        this.b = b;
    }
}
