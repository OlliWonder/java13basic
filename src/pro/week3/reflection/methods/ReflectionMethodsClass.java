package pro.week3.reflection.methods;

import pro.week2.task5.ConvertHashSet;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;

public class ReflectionMethodsClass {
    public static void main(String[] args) {
        //Задача 4.
        //Сконструировать класс.
        //То есть вызвать конструктор класса и обработать все возможные ошибки.
        Class<Task4> cls = Task4.class;
        try {
            Constructor<Task4> constructor = cls.getDeclaredConstructor(int.class, String.class);
            Task4 result = constructor.newInstance(143, "Test Constructor Reflection");
    
            //TODO: Add toString in Task4 class
            System.out.println(result);
            System.out.println(result.a);
            System.out.println(result.b);
            
        } catch (NoSuchMethodException e) {
            System.out.println("No such method: " + e.getMessage());
        }
        catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
