package pro.week3.reflection.modifiers;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

//позволяет узнать все модификаторы, с которыми был объявлен класс
//Модификаторы пакуются внутри битов инта, распаковать информацию можно методами класса Modifier
public class ReflectionClassModifiers {
    public static void main(String[] args) throws IllegalAccessException {
        //System.out.println(Modifier.isPublic(ReflectionClassModifiers.class.getModifiers()));
        //printAllFields(Task2.class);
        Task2 task2 = new Task2();
        printAllFieldsWhithValues(Task2.class, task2);
    }
    
    //Задача 2
    //Вывести все поля класса, их модификаторы и типы.
    public static void printAllFields(Class<?> clazz) {
        for (Field field : clazz.getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPublic(mods)) {
                System.out.println("public ");
            }
            if (Modifier.isProtected(mods)) {
                System.out.println("protected ");
            }
            if (Modifier.isPrivate(mods)) {
                System.out.println("private ");
            }
            if (Modifier.isStatic(mods)) {
                System.out.println("static ");
            }
            if (Modifier.isFinal(mods)) {
                System.out.println("final ");
            }
            System.out.println(field.getType().getName());
        }
    }
    
    //Задача 3
    //Продолжение задачи 2. Создать инстанс класса Task вывести значения его полей.
    private static void printAllFieldsWhithValues(Class<?> clazz, Task2 task2) throws IllegalAccessException {
        printAllFields(clazz);
        for (Field field : clazz.getDeclaredFields()) {
            int mod = field.getModifiers();
            if (Modifier.isPrivate(mod)) {
                field.setAccessible(true);
            }
            System.out.println(field.get(task2));
        }
        
    }
}

