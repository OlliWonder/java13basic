package pro.week3.reflection.classname;

import java.util.AbstractMap;
import java.util.HashMap;

public class ReflectionClassName {
    public static void main(String[] args) {
        printNamesForClass(String.class, "String.class");
        printNamesForClass(java.util.HashMap.SimpleEntry.class,
                "java.util.HashMap.SimpleEntry.class (nested class)");
        printNamesForClass(new java.io.Serializable() {}.getClass(),
                "java.io.Serializable(){}.getClass() (anonymous class)");
        
    }
    
    public static void printNamesForClass(Class<?> clazz, String label) {
        System.out.println(label + ":");
        System.out.println("getName(): " + clazz.getName());
        System.out.println("getSimpleName(): " + clazz.getSimpleName());
        System.out.println("getPackage().getName(): " + clazz.getPackage().getName());
        System.out.println("getType(): " + clazz.getTypeName());
        System.out.println();
    }
}
