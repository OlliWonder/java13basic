package pro.week3.reflection.superclass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReflectionSuperClass {
    public static void main(String[] args) {
        //вывести для класса D все интерфейсы, которые явно описаны в данном классе
//        for (Class<?> cls : D.class.getInterfaces()) {
//            System.out.println(cls.getName());
//        }
        
        //решение задачи 1
        List<Class<?>> result = getAllInterfaces(D.class);
        for (Class<?> cls : result) {
            System.out.println(cls.getName());
        }
    }
    
        //Задача 1
        //Получить все интерфейсы класса, включая интерфейсы от классов-родителей.
    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}
