package pro.week3.reflection;

public class Reflection {
    public static void main(String[] args) {
        // Способы получения объекта класса Class
        //1 способ
        // Исп псевдополе (неявное поле) .class
        Class<String> c1 = String.class;
        
        //2 способ
        // вызвать метод getClass() у Object
        // этот метод учитывает полиморфизм и возвращает реальный класс, которым является объект
        CharSequence sequence = "My String";
        Class<? extends CharSequence> c2 = sequence.getClass();
        System.out.println(c2);
        
        //3 способ
        //найти этот класс по строчному имени
        try {
            Class<?> integerClass = Class.forName("java.lang.Integer");
        }
        catch (ClassNotFoundException e) {
            System.out.println("Не могу найти такой класс");
        }
    }
}
