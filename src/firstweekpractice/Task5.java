package firstweekpractice;
/*
        Напишите аналог функции swap, которая меняет значения двух параметров местами (без вспомогательной переменной)
        Входные данные
        a = 8; b = 10
     */
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println(a + " " + b);
    }
}
