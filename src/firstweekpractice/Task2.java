package firstweekpractice;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println("Стоимость 1ГБ трафика: " + c * 1.0 / m);
        System.out.println("Стоимость 1ГБ трафика(v2): " + (double) c / m);
        System.out.println("Стоимость 1ГБ трафика(v3): " + c * 1D / m);
    }
}