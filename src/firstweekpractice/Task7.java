package firstweekpractice;
/*
     Перевод литров в галлоны. С консоли считывается число a - количество литров, нужно перевести это число в галлоны.
     (1 литр = 0,219969 галлона)
     */
import java.util.Scanner;
public class Task7 {
    public static final double LITRES_IN_GALLON = 0.219969;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        double res = a * LITRES_IN_GALLON;
        System.out.println(res);
    }
}