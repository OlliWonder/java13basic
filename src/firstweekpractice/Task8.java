package firstweekpractice;
/*
Дано целое число n.
        Выведите следующее за ним четное число.
        При решении этой задачи нельзя использовать условную инструкцию if и циклы.

        5 -> 6
        10 -> 12
        */
import java.util.Scanner;
public class Task8 {
    public static final double LITRES_IN_GALLON = 0.219969;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int next = (n / 2 + 1) * 2; // второе решение int res1 = n + 2 - Math.abs(n) % 2;
        System.out.println("Следующее чётное число: " + next);
    }
}