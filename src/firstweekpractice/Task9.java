package firstweekpractice;
/*
     Даны целые числа a, b и с, определяющие квадратное уравнение. Вычислить дискриминант.

     Подсказка: D = b^2 - 4 * a * c

     Входные данные
     a = 6 b = -28 с = 79
     */
import java.util.Scanner;
public class Task9 {
    public static final double LITRES_IN_GALLON = 0.219969;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        double d = Math.pow(b, 2) - 4 * a * c;
        System.out.println("Дискриминант равен: " + d);
    }
}