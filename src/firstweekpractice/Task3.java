package firstweekpractice;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        final int X0 = 0;
        final int Y0 = 0;
        Scanner input = new Scanner(System.in);
        double x = input.nextDouble();
        double y = input.nextDouble();
        double distance = Math.pow((Math.pow(x, 2) + Math.pow(y, 2)), 1.0 / 2); // если по-другому double res = Math.sqrt(Math.pow(x, 2) + y * y);
        System.out.println(distance);

    }
}
